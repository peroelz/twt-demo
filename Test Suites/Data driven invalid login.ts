<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Data driven invalid login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e3c4cda8-087b-4964-8609-e6cb8f87f1e8</testSuiteGuid>
   <testCaseLink>
      <guid>283f4797-5f76-4de9-8e02-33b1a48d1be5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data Driven Login/Login with unregistered credentials</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2ffbf4ed-b8af-4af7-9ab0-b0005be92ab0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/invalidCredential</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>2ffbf4ed-b8af-4af7-9ab0-b0005be92ab0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>uname</value>
         <variableId>65e68cb1-0fbf-4acc-9779-dd2ef7c45829</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2ffbf4ed-b8af-4af7-9ab0-b0005be92ab0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>6e052015-d51f-4c60-abd5-87777389c12e</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
