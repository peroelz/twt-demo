<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>da06dff5-af25-4413-8560-750658680a3f</testSuiteGuid>
   <testCaseLink>
      <guid>ad75cd2e-71b5-4d89-a7cd-70c8d7ef7af1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login valid credentials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5232e4b5-d4a0-4a16-830d-9274b4b3c2a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login with invalid password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2bcee94-1a59-410b-9dc1-2cb191b18d22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login with unregistered credentials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb3976dd-782e-4f74-bd4b-ec32915a7f3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Post/Post text</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bfa88fd2-75a8-48b8-80e4-3dbb51f85b3b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>351c7be9-369a-454e-87a7-dc0e67ca504c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Post/Post image with text</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0b13b70e-bded-437c-9ae0-cfeb356a23b5</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
