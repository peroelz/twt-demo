import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

def img = RunConfiguration.getProjectDir() + '/Manifesto/image.jpeg'

def imgpath = img.replace('/', '\\')

println(imgpath)

WebUI.callTestCase(findTestCase('Login/Login valid credentials'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Dashboard/div_Whats happening'))

'memanggil variable yang berisi template text'
WebUI.setText(findTestObject('Dashboard/textfield_post'), var_template)

'Memanggil custom keyword untuk handle native explorer. File yang dipanggil ada di /Manifesto/image.jpeg'
CustomKeywords.'custom.upload.uploadFile'(findTestObject('Dashboard/attach_img'), imgpath)

WebUI.click(findTestObject('Dashboard/tweet button'))

'mengecek postingan sukses/tidak, by observing //*[@id="react-root"]/div/div/div[2]/main/div/div/div/div/div/div[4]/div/div/section/div/div/div[4]/div/div/article/div/div/div/div[2]/div[2]/div[2]/div[1]/div/span'
WebUI.verifyElementVisible(findTestObject('Dashboard/newest post'))

'get text di postingan yang baru saja dibuat'
pos1 = WebUI.getText(findTestObject('Dashboard/text_firstpost'))

println(pos1)

'Untuk mengecek, kalimat di postingan yang terbaru sudah sesuai dengan apa yg di tulis user atau belum'
WebUI.verifyMatch(pos1, var_template, true)

