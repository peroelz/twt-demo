import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://twitter.com/')

WebUI.click(findTestObject('Homepage/homepage_login_btn'))

'wait until credential field appear\r\n'
WebUI.waitForElementVisible(findTestObject('Login Page/input_Password'), 15)

WebUI.click(findTestObject('Login Page/input_Phone'))

WebUI.setText(findTestObject('Login Page/input_Phone'), '085886080467')

WebUI.click(findTestObject('Login Page/input_Password'))

WebUI.setEncryptedText(findTestObject('Login Page/input_Password'), '98FTFqhkf+jEfG+NiNCYrg==')

WebUI.click(findTestObject('Login Page/div_Log in'))

'Make sure that error message "We could not verify your credentials." appear\r\n'
WebUI.waitForElementVisible(findTestObject('Login Page/err_wrong pwd'), 15)

