**SETUP:**
----------------------------
**Katalon can be downloaded at https://www.katalon.com/download/**
------------------
**Configuration to clone this project**

1. **Enable Git Integration:** To access all Git features, you need to enable Git Integration first. 
The option is available in the following settings: **Katalon Studio > Preferences > Katalon > Git**. 

    ![alt text](/Manifesto/Enable-Git-integration-in-Katalon-Studio-2.png)

    Once enabled, you can start using Git at Katalon Studio's main toolbar.

2. **Now the Git integration feature should be enabled. We are ready to use Git from Katalon Studio.**

    ![alt text](/Manifesto/git-icon.png)

3. **From the main toolbar, select the Git icon > Clone Project.**


4. **The Source Git Repository dialog is displayed.**

    Enter all required information and click Next to let Katalon Studio get details about your repository.
    ![alt text](/Manifesto/https.png)

    Where:

-     Repository URL: the remote URL to this Git repository in HTTPS protocol. https://gitlab.com/peroelz/twt-demo.git
-     Username: the username to access the Git repository.
-     Password: the password to access the Git repository.
**5. Next**


--------------------------
**TEST CASE EXPLANATION:**

Login with valid credential

    User & password inside testcase are hardcoded 
    If the login proccess success, it will redirect user into dashboard
    To verify that, i assert some element which available after logged in
    which is post tweet feature. So i make assertion to that element for verify

   ```
 //div[@id='react-root']/div/div/div[2]/main/div/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/
    div/div
```

Login with unregistered credential

    User & password inside testcase are hardcoded. 
    If the login proccess unsuccess, it will return error message
    To verify that, i assert element which contain text 
    "The email and password you entered did not match our records. Please double-check and try again."

Data Driven - Login with unregistered credential

    User & password inside testcase are dynamic. 
    it will call variable "uname & password"
  
Login with invalid password
    
    User & password inside testcase are hardcoded. 
    If the login proccess unsuccess, it will return error message
    To verify that, i assert element which contain text 
    "We could not verify your credentials. Please double-check and try again."


Post text

    First, i call previous test case which named "login with valid credentials"
    (Because to post some tweet, user should be logged in.)
    After that i create variable "Env_textpost" inside current testcase which will be used for template. 
    I add some assertion to check whether it succesfully posted or not, and make comparation between the template and the created twitter post.


Post image with text   

    This testcase similar with "post text"
    The difference is, i add custom keyword (named uploadfile) for handling file explorer.
    It will handle file selection by pressing CTRL C , CTRL V with value of environment "imgpath"
    Image file was included inside the project dir.
    In some case this may not working, U can change "imgpath" with the image directory based on your local file.
    
    

![alt text](/Manifesto/file.jpg)



-------
**TEST SUITE EXPLANATION:**

**Regression:**
It contain all test case , except **Data Driven Login/Login with unregistered credentials**

**Data driven invalid login:**
It contain testcase for data driven , Which will looping depends on the data file.

It will call testcase `Test Cases/Data Driven Login/Login with unregistered credentials`

Then passing variable binding from internal data file named invalidCredentials.

Test suite will looping based on data length

![alt text](/Manifesto/datadriven.jpg)




**HOW TO RUN TEST SUITE**

_Pre requisite:_

- Make sure that you have good internet connection
- Katalon installed and running properly
- Project already cloned into local directory
-------------

1. Launch Katalon Studio and open the project
2. Open the project
3. Select Test SUITE
4. Double click test suite file
5. Select "Play" button inside katalon navigatio menu
6. Select chrome

![alt text](/Manifesto/testsuite.jpg)




